import express from 'express';
const router = express.Router();
import {getProducts, getProductById,createProductReview, getProductByName, deleteProduct, createProduct, updateProduct, getTopProducts} from '../controllers/productController.js';
import { protect, admin } from '../middleware/authMiddleware.js';

// @desc Fetch all products
// @route GET /api/products
// @access Public

// router.get('/', getProducts);
//OR
router.route('/').get(getProducts).post(protect, admin, createProduct);

router.get('/top' ,getTopProducts);


// @desc Fetch single product
// @route GET /api/products/:id
// @access Public

// router.get('/:id', getProductById);
//OR
router.route('/:id').get(getProductById).delete(protect, admin, deleteProduct).put(protect, admin, updateProduct);

// @desc Search products
// @route GET /api/products/:productName
// @access Public

router.route('/productName/:productName').get(getProductByName)


router.route('/:id/reviews').post(protect, createProductReview);


export default router;